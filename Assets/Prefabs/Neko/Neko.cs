﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Neko : MonoBehaviour {

    public PlayableDirector director;
    public PlayableAsset[] items;
    public float speed = 1f;

    public Transform kubi;


    // Use this for initialization
    void Start () {
        director = gameObject.GetComponent<PlayableDirector>();
        setFlip();
        run();
    }
	
	// Update is called once per frame
	void Update () {
    }

    public void walk()
    {
        director.playableAsset = items[0];
        director.Play();
        //director.playableGraph.GetRootPlayable(0).SetSpeed(speed);
    }

    public void run()
    {
        director.playableAsset = items[1];
        director.Play();
        //director.playableGraph.GetRootPlayable(0).SetSpeed(3f);
    }

    public void setFlip()
    {
        kubi.Rotate(new Vector3(0f,180f,0f));
    }

}
