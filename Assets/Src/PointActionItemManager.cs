﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointActionItemManager : MonoBehaviour {

    [Header("Pointでのチェックの管理を行う")]

    public bool isDragging = false;
    public PickUpObject pickupObject;
    
    public PointActionSetting[] actionData;

    void Start()
    {
        actionData = gameObject.GetComponents<PointActionSetting>();
    }

    // 宣言されている　Contentを取得して　PointActionSettingを取得　してSettingメソッドを呼び出す
    // AddComponentされているPointActionSettingは継承されたデータ

    public void Action(NekoMove neko)
    {
        if (this.isDragging)
        {
            return;
        }

        if(this.actionData == null)
        {
            //Debug.Log("actionData == null");
            return;
        }

        for (int i=0; i < this.actionData.Length; i++)
        {
            if (this.actionData[i].isSetDataActive)
            {
                this.actionData[i].SetData(this.transform.parent.gameObject, neko);
            }
        }
    }
    public void SetActionDataActive(bool isSetDataActive)
    {
        for (int i = 0; i < this.actionData.Length; i++)
        {
            this.actionData[i].isSetDataActive = isSetDataActive;
        }
    }
}
