﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PickUpObject : MonoBehaviour
{
    public bool isDragEnable = false;

    public bool isDragging = false;
    public PointActionItemManager point;

    public SpriteRenderer sprite;

    private void Start()
    {
        this.point = gameObject.GetComponentInChildren<PointActionItemManager>();
        this.point.pickupObject = this;

        sprite = gameObject.GetComponent<SpriteRenderer>();

        setDragEnable(true);
    }

    public void setDragEnable(bool _setEnable)
    {
        isDragEnable = _setEnable;

        if (isDragEnable)
        {
            sprite.color = new Color(1f,1f,1f);
        }
        else
        {
            sprite.color = new Color(0.5f, 0.5f, 0.5f);
        }
    }

    public void OnDrag(BaseEventData data)
    {
        if (!isDragEnable) { return; }
        this.isDragging = true;
        this.point.isDragging = true;

        var eventData = (PointerEventData)data;
        transform.position = eventData.pointerCurrentRaycast.worldPosition;
    }

    public void OnEndDrag(BaseEventData data)
    {
        this.isDragging = false;
        this.point.isDragging = false;
    }
}
