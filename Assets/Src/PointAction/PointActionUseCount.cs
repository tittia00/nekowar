﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointActionUseCount : PointActionSetting
{
    public int Count = -1;
    public bool isAfterReset = false;

    private int resetCount = 0;
    private Vector3 resetPosition;

    private void Awake()
    {
        resetCount = Count;
        resetPosition = transform.parent.localPosition;
    }

    public override void SetData(GameObject thisObject, NekoMove neko)
    {
        if (Count == -1)
        {

        }
        else
        {
            Count--;
            if (0 >= Count)
            {
                if (isAfterReset)
                {
                    transform.parent.localPosition = resetPosition;
                    Count = resetCount;
                }
                else
                {
                    thisObject.SetActive(false);
                }
            }
        }
    }
}
