﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointCheckHaveItems : PointActionSetting
{
    /**
     * <summary>
     * 設定したGameObjectの名前でチェックする
     * </summary>
     */
    [Header("設定したGameObjectの名前で通過チェックする")]
    public bool isCheckByName = false;
    [Header("通過した場合持っているアイテムを消費する")]
    public bool isCostItem = false;

    public List<GameObject> hasCheckItems;

    /**
     * ここで設定されているアイテムをすべて持っているかチェックをする
     * 
     * 持っていない場合は特に何もしない
     * 　（ネコを反転させる別スクリプト　そのためこのチェックが初めになるように調整をする）
     * 持っている場合はグラフィックを変化させて通過できるオブジェクトにする
     * 
     */
    public override void SetData(GameObject thisObject, NekoMove neko)
    {

        if (neko.itemBox.HasItems(hasCheckItems, isCheckByName))
        {
            gameObject.transform.parent.gameObject.SetActive(false);
            // すべてアイテムを持っている
            // その他のPointActionをすべて停止する
            PointActionItemManager pickUpPoint = gameObject.GetComponent<PointActionItemManager>();
            pickUpPoint.SetActionDataActive(false);

            // 消費する場合
            if (isCostItem)
            {
                neko.itemBox.CostItems(hasCheckItems);
            }
        }
    }
}
