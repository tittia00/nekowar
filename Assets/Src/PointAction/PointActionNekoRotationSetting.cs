﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointActionNekoRotationSetting : PointActionSetting
{
    public float RotationAxis = 0;

    public override void SetData(GameObject thisObject, NekoMove neko)
    {
        neko.axis = RotationAxis;
    }
}
