﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointActionReturnMove : PointActionSetting
{
    public override void SetData(GameObject thisObject, NekoMove neko)
    {

        neko.axis += 180.0f;

        if (neko.axis < -180.0f)
        {
            neko.axis += 360.0f;
        }
        else if (neko.axis > 180.0f)
        {
            neko.axis -= 360.0f;
        }
    }
}
