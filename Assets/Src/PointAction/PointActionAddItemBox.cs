﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointActionAddItemBox : PointActionSetting
{

    public GameObject addItemBoxObject;
    public override void SetData(GameObject thisObject, NekoMove neko)
    {
        neko.AddItem(addItemBoxObject);
    }
}
