﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpManager : MonoBehaviour {

    PickUpObject[] pickups;

	// Use this for initialization
	void Start () {
        pickups = gameObject.GetComponentsInChildren<PickUpObject>();
    }
	
	// Update is called once per frame
	public void Play (bool start) {
        if (start)
        {
            for(int i=0; i< pickups.Length; i++)
            {
                pickups[i].setDragEnable(false);
            }
            
        }
        else
        {

            for (int i = 0; i < pickups.Length; i++)
            {
                pickups[i].setDragEnable(true);
            }
        }

    }
}
