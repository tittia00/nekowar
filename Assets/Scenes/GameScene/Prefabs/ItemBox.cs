﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBox : MonoBehaviour {

    /*
     * PointActionAddItemBox
     * NekoMove
     * ItemBox
     * 
     * ItemBoxで取得したアイテムを保持
     * 
     * 保持する際
     * 　追加された_addItemをhasItemsに保持
     * 　_addItemをフレーム内に収まるように位置調整
     * 　　保持している個数によって綺麗に並ぶように調整
     */

    public List<GameObject> hasItems;

    //public List<GameObject> hasCheckItems;
    public Transform items;

    public void AddItem(GameObject _addItem)
    {
        hasItems.Add(_addItem);
        _addItem.transform.parent = items;
        AlignmentItems();
    }


    public bool HasItems(List<GameObject> checkItems,bool isCheckByName)
    {
        if (isCheckByName)
        {
            return HasItemsByName(checkItems);
        }
        else
        {
            return HasItems(checkItems);
        }
    }

    /**
     * 所持アイテムの削除
     */
    public void CostItems(List<GameObject> checkItems)
    {
        foreach (GameObject checkItem in checkItems)
        {
            for (int i = 0; hasItems.Count > i; i++)
            {
                if (hasItems[i].name.Contains(checkItem.name))
                {
                    // 所持アイテムを削除する
                    hasItems[i].SetActive(false);
                    hasItems.Remove(hasItems[i]);
                    
                }
            }
        }
        AlignmentItems();
    }

    /**
    * checkItemsのアイテムをすべて持っているかチェックを行う
    * 持っている場合はtrueを返す
    */
    private bool HasItems(List<GameObject> checkItems)
    {
        // チェックするアイテムの数が持っているアイテムの数以上の場合はすべてそろっていないのでfalse
        if (checkItems.Count > hasItems.Count) { return false; }

        // ひとつでも持っていなかった場合はfalse
        foreach (GameObject checkItem in checkItems)
        {
            if (!hasItems.Contains(checkItem))
            {
                return false;
            }
        }

        // すべてあったのでtrue
        return true;
    }
    private bool HasItemsByName(List<GameObject> checkItems)
    {
        // チェックするアイテムの数が持っているアイテムの数以上の場合はすべてそろっていないのでfalse
        if (checkItems.Count > hasItems.Count) { return false; }


        List<GameObject> hasCheckItems = new List<GameObject>(hasItems);

        // ひとつでも持っていなかった場合はfalse
        foreach (GameObject checkItem in checkItems)
        {
            bool isHasItem = false;
            for (int i=0; hasCheckItems.Count >i ;i++)
            {
                if (hasCheckItems[i].name.Contains(checkItem.name))
                {
                    isHasItem = true;

                    // 一度チェックしたアイテムは外すことで複数個のチェックを行う
                    hasCheckItems.Remove(hasCheckItems[i]);
                }
            }

            // チェックしたアイテムを持っていなかった場合
            if (!isHasItem)
            {
                return false;
            }
        }

        // すべてあったのでtrue
        return true;
    }

        // 綺麗に並べる
    private void AlignmentItems()
    {
        // 1つもない時は処理しない
        if (hasItems.Count <= 0)
        {
            return;
        }



        RectTransform size = this.GetComponent<RectTransform>();
        float Width = size.sizeDelta.x;

        int MaxSize = hasItems.Count + 1;

        //int MaxSize = 5+1;

        float spaceWidth = Width / MaxSize;

        for (int i=1; i<MaxSize ;i++)
        {
            Debug.Log(spaceWidth * i);
            hasItems[i - 1].transform.localPosition = new Vector3(spaceWidth * i,0,0);
        }
    }
}
