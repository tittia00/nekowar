﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePath : MonoBehaviour {

    public GameObject[] movePathObject;
    private Component[] currentPath;
    private int currentPathNum = 1;

    // Use this for initialization
    void Start () {

        currentPath = movePathObject[0].GetComponentsInChildren(typeof(Transform));
        NextMove();

    }
	
	// Update is called once per frame
	void Update () {
		
	}


    /*
     * 次の移動先の設定
     */
    void NextMove()
    {
        bool RandMove = false;
        float speed = 1.0f;

        Hashtable hash = iTween.Hash();

        // 位置作成
        SetMovePosition(hash, RandMove);

        {   // 速度の設定
            float nextDist = Vector3.Distance(((Transform)currentPath[currentPathNum]).position, transform.position);
            hash.Add("time", nextDist / speed); // メソッドがあるオブジェクトを指定
        }

        {
            // 移動が終わった時の処理の設定
            hash.Add("oncompletetarget", gameObject); // メソッドがあるオブジェクトを指定
            hash.Add("oncomplete", "MoveEnd"); // 実行するタイミング、実行するメソッド名
        }
        iTween.MoveTo(gameObject, hash);
    }

    /*
     * 移動が終わった時
     */
    void MoveEnd()
    {
        // 次の移動先を決める
        currentPathNum++;

        if (currentPath.Length-1 < currentPathNum)
        {
            currentPathNum = 1;
        }


        NextMove();
    }


    /**
     * Hashtable hash x,yを設定する
     * 
     */
    void SetMovePosition(Hashtable hash,bool RandMove)
    {
        float movex = 0;
        float movey = 0;
        {
            Transform tmp = (Transform)currentPath[currentPathNum];
            movex = tmp.position.x;
            movey = tmp.position.y;
            if (RandMove)
            {
                movex += Random.Range(-1f, 1f);
                movey += Random.Range(-1f, 1f);
            }
        }
        hash.Add("x", movex);
        hash.Add("y", movey);
    }
}
