﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnClickDispItem : MonoBehaviour {

    private GameObject DispTarget;
    private Camera cam;
    private DispFrameSprits dispFrameSprite;

    private Vector3 viewPos;
    private Vector3 pos;


    [Header("GameObjectのSpriteRenderer:tag:ClickDispImageを全て使用する")]
    public GameObject itemLists;


    private SpriteRenderer[] setDispItems;

    private void Awake()
    {
        DispTarget = GameObject.Find("Canvas/DispFrame");
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        dispFrameSprite = GameObject.Find("Canvas/DispFrame").GetComponent<DispFrameSprits>();

    }

    private void Start()
    {

        setDispItems = itemLists.GetComponentsInChildren<SpriteRenderer>();
    }

    public void SetDisplay()
    {
        gameObject.SetActive(true);

        //ここで通過不能マスの必要条件を表示する

        viewPos = cam.WorldToViewportPoint(this.transform.position);

        print(960-(960* viewPos.x));

        pos = DispTarget.transform.localPosition;

        pos.x = (960 * viewPos.x) - 480;
        pos.y = (600 * viewPos.y) - 300 + 60;

        DispTarget.transform.localPosition = pos;

        dispFrameSprite.setImages(setDispItems);
    }
}
