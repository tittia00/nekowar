﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DispFrameSprits : MonoBehaviour {

    public GameObject itemLists;
    public Image[] dispImageItems;

    FadeInOut fade;


    // Use this for initialization
    void Awake () {
        dispImageItems = itemLists.GetComponentsInChildren<Image>();

        for (int i = 0; dispImageItems.Length > i; i++)
        {
            dispImageItems[i].gameObject.SetActive(false);
        }

        fade = gameObject.GetComponent<FadeInOut>();

        if(fade == null)
        {
            Debug.LogAssertion("DispFrameSprite fade is null. GameObject create FadeInOut.");
        }
    }

    private void Start()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    public void setImages(Image[] _setDispItems)
    {
        gameObject.SetActive(true);

        for (int i = 0; _setDispItems.Length > i && dispImageItems.Length > i; i++)
        {
            dispImageItems[i].gameObject.SetActive(true);
            dispImageItems[i].sprite = _setDispItems[i].sprite;
            dispImageItems[i].color = _setDispItems[i].color;
        }

        fade.Reset();
        fade.SetFead(FadeInOut.FeadState.IN, true);

        StartCoroutine(dispTime());
    }
    public void setImages(SpriteRenderer[] _setDispItems)
    {
        gameObject.SetActive(true);

        for (int i = 0; _setDispItems.Length > i && dispImageItems.Length > i; i++)
        {
            if (_setDispItems[i].gameObject.tag == "ClickDispImage")
            {
                dispImageItems[i].gameObject.SetActive(true);
                dispImageItems[i].sprite = _setDispItems[i].sprite;
                dispImageItems[i].color = _setDispItems[i].color;
            }
        }

        fade.Reset();
        fade.SetFead(FadeInOut.FeadState.IN, true);

        StartCoroutine(dispTime());
    }

    IEnumerator dispTime()
    {
        yield return new WaitForSeconds(1);
        fade.SetFead(FadeInOut.FeadState.OUT, false);
    }
}
