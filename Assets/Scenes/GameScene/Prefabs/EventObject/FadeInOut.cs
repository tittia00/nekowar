﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeInOut : MonoBehaviour {

    public enum FeadState
    {
        END,
        IN,
        OUT
    };

    FeadState state = FeadState.END;

    float speed = 0.05f;
    float alfa = 0;
    bool setActiveFlag;

    public Image[] targetImages;

    private void Awake()
    {
        targetImages = GetComponentsInChildren<Image>();
        state = FeadState.END;
    }

    public void Reset()
    {
        targetImages = GetComponentsInChildren<Image>();
        state = FeadState.END;
    }


    public void SetFead(FeadState setState,bool _SetActive)
    {
        setActiveFlag = _SetActive;
        SetFead(setState);
    }

    private void SetFead(FeadState setState)
    {
        if (setState == FeadState.IN)
        {
            alfa = 0f;
        }
        else if (setState == FeadState.OUT)
        {
            alfa = 1f;
        }
        state = setState;
        SetTargetImageColorAlfa();
    }

    // Update is called once per frame
    void Update() {
        switch (state)
        {
            case FeadState.IN:
                alfa += speed;
                if (alfa >= 1)
                {
                    alfa = 1;
                    gameObject.SetActive(setActiveFlag);
                    state = FeadState.END;
                }
                SetTargetImageColorAlfa();
                break;
            case FeadState.OUT:
                alfa -= speed;
                if (alfa <= 0)
                {
                    alfa = 0;
                    gameObject.SetActive(setActiveFlag);
                    state = FeadState.END;
                }
                SetTargetImageColorAlfa();
                break;
        }
    }

    void SetTargetImageColorAlfa() {

        for (int i = 0; i < targetImages.Length; i++)
        {
            Color c = targetImages[i].color;
            c.a = alfa;
            targetImages[i].color = c;
        }
    }
}
