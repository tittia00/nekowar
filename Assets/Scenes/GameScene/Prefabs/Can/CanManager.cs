﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanManager : MonoBehaviour {

    private GameObject icon;
    private SpriteRenderer iconSpriteRender;
    private Image itemBoxImage;

    // Use this for initialization
    private void Awake()
    {
        icon = GameObject.Find("ItemsList/" + gameObject.name+"/icon");
        iconSpriteRender = icon.GetComponent<SpriteRenderer>();

        itemBoxImage = gameObject.GetComponentInChildren<Image>();

        itemBoxImage.sprite = iconSpriteRender.sprite;
    }
}
