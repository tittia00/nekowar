﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NekoFloorHitPoint : MonoBehaviour {

    NekoMove neko;

	// Use this for initialization
	void Start () {
        neko = gameObject.transform.parent.gameObject.GetComponent<NekoMove>();
    }
	
	// Update is called once per frame
	void Update () {

    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("OnTriggerEnter2D");
        //Debug.Log("OnTriggerEnter2D:" + collision.gameObject.tag);
        if (collision.gameObject.tag == "UniqueFloorPoint")
        {
            neko.HitUniquePoint(collision.gameObject);
        }
    }
}
