﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;

public class ResultButtonScript : MonoBehaviour {

    [DllImport("__Internal")]
    private static extern void OpenWindow(string str);

    public static int point = 0;

    public void Home()
    {
        //soundManager.Play(SoundManager.SE.CLICK);
        OpenWindow("http://tiatia.bambina.jp/game/NekoAtsuiUnity1Week/asset.html");
    }
    public void Twitter()
    {
        //soundManager.Play(SoundManager.SE.CLICK);
        //Hello();
        if(point == 0)
        {
            OpenWindow("http://twitter.com/ngntrtr/status/1038414109866254342");
        }
    }

    public void Ranking()
    {
        naichilab.RankingLoader.Instance.SendScoreAndShowRanking(point);
    }

    public void ResetGame()
    {
        SceneManager.LoadScene("TitleScene", LoadSceneMode.Single);
    }
}
