﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NekoMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
        ReStart();
    }
    public float speed = 0;
    public float axis = 0;

    public Transform startTransform;
    public GameObject[] ResultDispObjects = null;

    public ItemBox itemBox;

    
    public void StateChangeDispObject(GameObject[] _targets,bool _active)
    {
        for (int i=0; i< _targets.Length; i++)
        {
            _targets[i].SetActive(_active);
        }
    }

    public void NekoPlay(float _speed)
    {
        speed = _speed;
    }

    public void ReStart()
    {
        speed = 0;
        axis = 0;
        gameObject.transform.localPosition = startTransform.localPosition;
        gameObject.transform.rotation = startTransform.rotation;
        StateChangeDispObject(ResultDispObjects,false);
    }


    public void AddItem(GameObject _addItem)
    {
        itemBox.AddItem(_addItem);
    }


    // Update is called once per frame
    void Update () {
        Quaternion rotation = Quaternion.AngleAxis(this.axis, -Vector3.forward);
        Vector3 velocity = rotation * new Vector3(this.speed, 0, 0);
        gameObject.transform.position += velocity * Time.deltaTime;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("OnCollisionEnter2D");
        //Debug.Log("OnCollisionEnter2D tag :" + collision.gameObject.tag);
        //Debug.Log("OnCollisionEnter2D name:" + collision.gameObject.name);

        if (collision.gameObject.tag == "IgnoreHit")
        {
            return;
        }
        if (collision.gameObject.tag == "UniqueFloor")
        {
            return;
        }

        Debug.Log("OnCollisionExit2D:" + collision.gameObject.tag);


        if (collision.gameObject.tag == "Goal")
        {
            speed = 0;
            StateChangeDispObject(ResultDispObjects, true);
        }
        else if(collision.gameObject.tag == "Wall")
        {
            Quaternion rotation = Quaternion.AngleAxis(this.axis, -Vector3.forward);
            Vector3 velocity = rotation * new Vector3(-this.speed, 0, 0);
            gameObject.transform.position += velocity * Time.deltaTime;

            axis += 90.0f;

        }
    }
    public void OnCollisionExit2D(Collision2D collision)
    {
        //Debug.Log("OnCollisionExit2D");
    }
    public void OnCollisionStay2D(Collision2D collision)
    {
        //Debug.Log("OnCollisionStay2D");
        if (collision.gameObject.tag == "Wall")
        {
            axis += 90.0f;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("OnTriggerEnter2D");
        //Debug.Log("OnTriggerEnter2D:"+ collision.gameObject.tag);
        //if (collision.gameObject.tag == "UniqueFloorPoint")
        //{
        //    Debug.Log("OnTriggerEnter2D");
        //}
    }

    public void HitUniquePoint(GameObject collisionObject)
    {

        PointActionItemManager hitPoint = collisionObject.GetComponent<PointActionItemManager>();

        if(hitPoint != null)
        {
            hitPoint.Action(this);
        }
        else
        {
            Debug.Log(""+ collisionObject.name);
        }
    }

    public void OnTriggerStay2D(Collider2D collision)
    {

        //Debug.Log("OnTriggerStay2D");
    }

    public void OnTriggerExit2D(Collider2D collision)
    {

        //Debug.Log("OnTriggerExit2D");
    }
}
