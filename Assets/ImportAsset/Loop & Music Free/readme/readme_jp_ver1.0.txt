//-------------------------------------------------------------------------------------
// Loop !! Free Trial Version
// @2018 marching dream
// Version 1.0
//-------------------------------------------------------------------------------------

Loop !! Free Trial Version は、Rock、Pops、Electronica、Ambient 等の様々な音楽ジャンルの
ループ音源をまとめた無料トライアル版です。
このアセットには以下の4つの音源が収録されています(ファイル形式はmp3)。

- Rock 01
(Full 3:36 + Intro 0:16 , Main 0:32)
- Pops 01
(Full 2:24 + Intro 0:16 , Main 0:16)
- Electronic 01
(Full 2:47 + Intro 0:18 , Main 0:18)
- Ambient 01
(Full 3:39 + Intro 0:27 , Main 0:54)


このアセットの音源が気に入ったなら、ぜひ公式サイトまたは公式SoundCloudにて他の音源もチェックしてみて下さい。

公式サイト
http://marchingdream.blog.jp

公式 Sound Cloud
https://soundcloud.com/marchingdream


Version 1.0 - Fist Release