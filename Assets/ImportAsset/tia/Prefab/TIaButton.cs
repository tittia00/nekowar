﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TIaButton : MonoBehaviour {

    public SoundManager.SOUND_ITEM soundType = SoundManager.SOUND_ITEM.CLICK;

    SoundManager soundManager = null;

    Button button;
	// Use this for initialization
	void Start () {
        button = gameObject.GetComponent<Button>();

        // マネージャーの準備
        if (soundManager == null)
        {
            GameObject soundManagerObject = GameObject.Find("SoundManager");

            if (soundManagerObject != null)
            {
                soundManager = soundManagerObject.GetComponent<SoundManager>();
            }
        }

        // 再生処理
        if (soundManager != null)
        {
            button.onClick.AddListener(() => soundManager.PlayOnce((int)soundType));
        }
    }
	
}
