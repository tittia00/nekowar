﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : SingletonMonoBehaviourFast<SoundManager>
{
    //public AudioSource[] audioItem;

    public enum SOUND_ITEM
    {
        BGM_TITLE,
        CLICK,
        MAX
    };

    public List<AudioSource> setAudioItems;

    private Dictionary<SOUND_ITEM, AudioSource> audioItems;
    public float setVolume = 0.0f;

    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);

        audioItems = new Dictionary<SOUND_ITEM, AudioSource>();
        if (((int)SOUND_ITEM.MAX) != setAudioItems.Count)
        {
            Debug.LogError("SoundManager SOUND_ITEMとsetAudioItemsの数が一致していません。 "+ setAudioItems.Count +"/"+(int)SOUND_ITEM.MAX);
        }

        for (int i=0; i< setAudioItems.Count; i++)
        {
            audioItems.Add((SOUND_ITEM)i,setAudioItems[i]);
        }
        setAudioItems.Clear();
        setAudioItems = null;

        ResetVolume(setVolume);
        PlayLoop(SOUND_ITEM.BGM_TITLE);
    }


    public void ResetVolume(float _setVolume)
    {
        setVolume = _setVolume;
        if (audioItems == null) { return; }
        foreach (AudioSource audioItem in audioItems.Values)
        {
            audioItem.volume = setVolume;
        }
    }

    public void PlayLoop(SOUND_ITEM playItem)
    {
        audioItems[playItem].loop = true;
        audioItems[playItem].Play();
    }
    public void PlayOnce(SOUND_ITEM playItem)
    {
        audioItems[playItem].loop = false;
        audioItems[playItem].Play();
    }
    public void PlayOnce(int playItem)
    {
        audioItems[(SOUND_ITEM)playItem].loop = false;
        audioItems[(SOUND_ITEM)playItem].Play();
    }


    public void PlayClick()
    {
        PlayOnce(SOUND_ITEM.CLICK);
    }
}
