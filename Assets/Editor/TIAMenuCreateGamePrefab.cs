﻿using System.IO;
using UnityEditor;
using UnityEngine;
using TIAMenuCreatePrefab;


namespace TIAMenuCreateGamePrefab
{
    internal class TIAMenuCreateGamePrefab
    {

        [MenuItem("TIAMenu/GamePrefab/ObjectBase")]
        private static void ObjectBase()
        {
            CreatePrefab("Assets/Scenes/GameScene/Prefabs/ObjectBase.prefab");
        }






        [MenuItem("TIAMenu/GamePrefab/Item/ItemBase")]
        private static void ItemBase()
        {
            CreatePrefab("Assets/Scenes/GameScene/Prefabs/Item/ItemBase.prefab");
        }

        [MenuItem("TIAMenu/GamePrefab/Item/Can/Saba")]
        private static void ItemCanSaba()
        {
            CreatePrefab("Assets/Prefabs/Can/SabaCan.prefab");
        }
        [MenuItem("TIAMenu/GamePrefab/Item/Can/Iwashi")]
        private static void ItemCanIwashi()
        {
            CreatePrefab("Assets/Prefabs/Can/IwashiCan.prefab");
        }
        [MenuItem("TIAMenu/GamePrefab/Item/Can/Chicken")]
        private static void ItemCanChicken()
        {
            CreatePrefab("Assets/Prefabs/Can/ChickenCan.prefab");
        }




        [MenuItem("TIAMenu/GamePrefab/Item/Bar/Saba")]
        private static void ItemBarSaba()
        {
            CreatePrefab("Assets/Prefabs/Bar/SabaBar.prefab");
        }
        [MenuItem("TIAMenu/GamePrefab/Item/Bar/SabaBarVerticalLong")]
        private static void ItemBarSabaBarVerticalLong()
        {
            CreatePrefab("Assets/Prefabs/Bar/SabaBarVerticalLong.prefab");
        }
        [MenuItem("TIAMenu/GamePrefab/Item/Bar/Iwashi")]
        private static void ItemBarIwashi()
        {
            CreatePrefab("Assets/Prefabs/Bar/IwashiBar.prefab");
        }
        [MenuItem("TIAMenu/GamePrefab/Item/Bar/ChickenBarHorizonLong")]
        private static void ItemBarChickenBarHorizonLong()
        {
            CreatePrefab("Assets/Prefabs/Bar/ChickenBarHorizonLong.prefab");
        }





        [MenuItem("TIAMenu/GamePrefab/PickUpUniqueFloor/UP")]
        private static void PickUpUniqueFloor_UP()
        {
            CreatePrefab("Assets/Scenes/GameScene/Prefabs/PickUp/PickUpUniqueFloor_UP.prefab");
        }
        [MenuItem("TIAMenu/GamePrefab/PickUpUniqueFloor/DOWN")]
        private static void PickUpUniqueFloor_DOWN()
        {
            CreatePrefab("Assets/Scenes/GameScene/Prefabs/PickUp/PickUpUniqueFloor_DOWN.prefab");
        }
        [MenuItem("TIAMenu/GamePrefab/PickUpUniqueFloor/LEFT")]
        private static void PickUpUniqueFloor_LEFT()
        {
            CreatePrefab("Assets/Scenes/GameScene/Prefabs/PickUp/PickUpUniqueFloor_LEFT.prefab");
        }
        [MenuItem("TIAMenu/GamePrefab/PickUpUniqueFloor/RIGHT")]
        private static void PickUpUniqueFloor_RIGHT()
        {
            CreatePrefab("Assets/Scenes/GameScene/Prefabs/PickUp/PickUpUniqueFloor_RIGHT.prefab");
        }




        [MenuItem("TIAMenu/GamePrefab/Wall/MoveWall")]
        private static void Wall_MoveWall()
        {
            CreatePrefab("Assets/Scenes/GameScene/Prefabs/Wall/MoveWall.prefab");
        }
        [MenuItem("TIAMenu/GamePrefab/Wall/Wall")]
        private static void Wall_Wall()
        {
            CreatePrefab("Assets/Scenes/GameScene/Prefabs/Wall/Wall.prefab");
        }




        private static GameObject CreatePrefab(string prefabPath)
        {
            GameObject prefab = AssetDatabase.LoadAssetAtPath(prefabPath, typeof(GameObject)) as GameObject;
            PrefabUtility.InstantiatePrefab(prefab);
            return prefab;
        }
    }
}